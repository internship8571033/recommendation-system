# Call Center Recommendation System README

This repository showcases my significant contributions during my internship, where I played a role in developing and refining a recommendation system for call center. Inside this repository, you will find the comprehensive collection of code, resources, and documentation on **master** branch that reflect my dedicated efforts and achievements throughout this project
## Table of Contents

- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Contributors](#contributors)


## Description

The Call Center Recommendation System is designed to enhance the call center experience by providing relevant recommendations for responding to customer questions.questions.My task employs the following key components:

1. **Data Cleaning:** Initial data preprocessing to ensure the quality and consistency of the input data.
2. **Clustering:** Grouping agent responses and customer questions into clusters to identify patterns and similarities.
3. **Natural Language Processing (NLP):** Leveraging NLP techniques to analyze and understand the content of agent responses and customer questions.
   

## Installation

To run the recommendation system, follow these steps:

1. Clone this repository to your local machine.
2. Make sure that you add your OpenAi API key in the .env file
3. Install the required dependencies by running the following command:
   
   ```bash
   pip install -r requirements.txt

## Usage
To use the recommendation system, follow these steps:

1. Ensure you have completed the installation steps mentioned above.
2. Run the `trained_bot_app_main.py `script to launch the recommendation system app.
3. Input customer questions or agent responses as prompted by the app.
4. Receive relevant recommendations based on clustering and NLP analysis.


## Contributors

**Akrem Benmbarek** Computer science engineering student at ENSI